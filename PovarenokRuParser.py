# -*- coding: utf-8 -*-
from lxml.html import parse


class PovarenokRuParser():
    """Сlass parses the page with the recipe on the website http://www.povarenok.ru/ (reprint of materials allowed only
    with active link to www.povarenok.ru)
    Сlass collects such data on the recipe:

    :param recipe_description: a short text description of the recipe from the author
    :param recipe_category: category to which the recipe (appetizers, main dishes, etc.)
    :param recipe_ingredients_list: list of ingredients that make up the recipe. contains a dictionary with the keys of
           ingredient_dosage and ingredient_name
    :param s_recipe_ingredients_list: list of ingredients that make up the recipe, made for easy retrieval in the
           database for igredients. ingredients dosage dose contains
    :param recipe_steps_list: cooking steps together with pictures of each step and text description
    :param recipe_videos: videos related to cooking process
    :param recipe_text: if the recipe contains step by step instructions, it contains a description protsesa cooking
    :param recipe_title: recipe name
    :param recipe_img_cover: picture, usually, the finished dish, which can be used as a cover

    :type recipe_description: unicode str
    :type recipe_category: unicode str
    :type recipe_ingredients_list: list
    :type s_recipe_ingredients_list: list
    :type recipe_steps_list: list
    :type recipe_videos: list
    :type recipe_text: unicode str
    :type recipe_title: unicode str
    :type recipe_img_cover: unicode str

    and stores them in the instance variables

    """

    def __init__(self, document_path):
        """When initializing the class collect all the necessary parts of the html page and declare the outcome
        variables

        :param document_path: name of the document that you want to parse

        :type document_path: str

        """
        self.recipe_origin = "http://www.povarenok.ru/"
        self.recipe_description = ""
        self.recipe_category = ""
        self.recipe_ingredients_list = []
        self.s_recipe_ingredients_list = []
        self.recipe_steps_list = []
        self.recipe_videos = []
        self.recipe_text = ""
        self.recipe_title = ""
        self.recipe_img_cover = ""
        self.recipe_type = ""

        self.html_page = parse(document_path).getroot()
        self.html_ingredients = self.html_page.cssselect('div.recipe-ing > ul > li')
        self.s_html_ingredients = self.html_page.cssselect('div.recipe-ing > ul > li a > span')
        self.html_description = self.html_page.cssselect('div.recipe-short > span')
        self.html_category = self.html_page.cssselect('div.recipe-infoline a')
        self.html_iframes = self.html_page.cssselect('iframe')
        self.html_step = self.html_page.cssselect('div.recipe-steps > table > tbody > tr > td')
        self.html_step_img_min = self.html_page.cssselect('div.recipe-steps > table > tbody > tr > td > a')
        self.html_step_img_max = self.html_page.cssselect('div.recipe-steps > table > tbody > tr > td > a > img')
        self.html_recipe_text = self.html_page.cssselect('div.recipe-text')
        self.html_title = self.html_page.cssselect('div#print_body > h1')
        self.html_img_cover = self.html_page.cssselect('div.recipe-img > img')

        self.add_ingredients()
        self.add_ingredients_for_search()
        self.add_description()
        self.add_category()
        self.add_title()
        self.add_img_cover()
        if self.html_step:
            self.add_steps()
        else:
            self.add_recipe_text()
        if self.html_iframes:
            self.add_videos()
        self.add_type()

    def add_ingredients(self):
        for i in self.html_ingredients:
            # Сложнопереводимый питонический сахар, поэтому по порядку ' '.join(i.text_content().split()).split(u'—'):
            # i.text_content() - достаем весь текст из объекта li (там много вложенностей, поэтому просто выдергиваем
            # текст)
            # i.text_content().split() - разбиваем строку по пробельным символам (табуляция, пробелы), получится массив
            # ' '.join(i.text_content().split()) - соединяем эелементы полученного массива в строку через пробел
            # ' '.join(i.text_content().split()).split(u'—') - разбиваем полученную строку по тире, получается массив
            ingr_components = ' '.join(i.text_content().split()).split(u'—')

            # Создаем ассоциативный массив (или хеш-таблицу, или словарь (dict) в питоне), избавляясь от пробельных
            # символов по краям строки
            ingredient = {}
            ingredient['ingredient_name'] = ingr_components[0].strip()
            try:
                ingredient['ingredient_dosage'] = ingr_components[1].strip()
            except IndexError:
                ingredient['ingredient_dosage'] = None

            # Добавляем словарь в конец списка
            self.recipe_ingredients_list.append(ingredient)

    def add_img_cover(self):
        self.recipe_img_cover = self.html_img_cover[0].get('src')

    def add_title(self):
        self.recipe_title = self.html_title[0].text_content().strip()

    def add_ingredients_for_search(self):
        for i in self.s_html_ingredients:
            if i.text.strip() not in [u'Сахар', u'Соль', u'Вода']:
                self.s_recipe_ingredients_list.append(i.text.strip())

    def add_recipe_text(self):
        self.recipe_text = self.html_recipe_text[0].text_content()

    def add_videos(self):
        # если в ссылке на источник iframe'a есть нижеперечисленные слова, то это видео и его можно отнести к рецепту
        # (еще могут быть всякие социальные кнопки, виджеты и т.д.)
        for i in self.html_iframes:
            src = i.get('src')
            if 'youtube' in src \
            or 'rutube' in src \
            or 'vimeo' in src:
                self.recipe_videos.append(src)

    def add_category(self):
        self.recipe_category = self.html_category[0].text

    def add_steps(self):
        # Это генератор списков (list comprehension) проходим в цикле по всем эелементам списка
        # (for i in self.html_step_img_min), получаем значение атрибута href и добавляем в конец уже списка step_img_min
        step_img_min = [i.get('href') for i in self.html_step_img_min]
        step_img_max = [i.get('src') for i in self.html_step_img_max]
        # Функция map() принимает как аргументы функцию и последовательность, к которой будет применять функцию,
        # переданную первым аргументом.
        step_text = []
        # map(lambda x: step_text.append(x.text.strip()) if x.text.strip() else False, self.html_step)
        for i in self.html_step:
            try:
                content = i.text.strip()
            except AttributeError:
                content = u""
            if content:
                step_text.append(content)

        # c = zip(lst1, lst1), где lst1 = [1, 2, 3], а lst2 = ['a', 'b', 'c']
        # делает с = [(1, 'a'), (2, 'b'), (3, 'c')]
        # Как работает этот цикл проще будет объяснить в скайпе
        for i_min, i_max, t in zip(step_img_min, step_img_max, step_text):
            step = dict()
            step['step_img_min'] = i_max
            step['step_img_max'] = i_min
            step['step_description'] = t
            self.recipe_steps_list.append(step)

    def add_description(self):
        try:
            self.recipe_description = self.html_description[0].text_content().strip()
        except IndexError:
            pass

    def add_type(self):
        if self.recipe_text and self.recipe_videos:
            self.recipe_type = 'video'
        elif self.recipe_steps_list and self.recipe_videos:
            self.recipe_type = 'comb'
        elif self.recipe_steps_list:
            self.recipe_type = 'sbs'
        elif self.recipe_text:
            self.recipe_type = 'text'